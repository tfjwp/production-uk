<?php

/**
 * User Registration Form: 
 * Reset the default user ID in the user ID field so that
 * a user is created instead of editing the current user.
 */
add_filter('frm_get_default_value', 'puk_reset_user_id', 10, 2);
function puk_reset_user_id($new_value, $field){
  if ( in_array( $field->id, array( 346 ) ) ) { // the userID field
    $new_value = '0';
  }
  return $new_value;
}
/*
A shortcode to allow conditional checking of fields. 
*/
function maybe_frm_value_func( $atts, $content = '' ) {
      $val = FrmProEntriesController::get_field_value_shortcode($atts);
      if($val == $atts['equals']){
        return $content;
      }else{
        return '';
      }
}
add_shortcode( 'maybe-frm-field-value', 'maybe_frm_value_func' );

?>