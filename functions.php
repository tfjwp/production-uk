<?php
/**
 * WP-Starter functions and definitions
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package WordPress
 * @subpackage WP_Starter
 * @since WP-Starter 4.0
 */

/**
 * Enqueue our child-theme style sheets
 */
function wpstarter_child_style() {
    wp_dequeue_style('wpforge');
    wp_enqueue_style('parent-styles', get_template_directory_uri() . '/style.css', '', '5.5.2.5');
    wp_enqueue_style('child-styles', get_stylesheet_uri(), array( 'parent-styles' ), '4.0');
}
add_action( 'wp_enqueue_scripts', 'wpstarter_child_style');

require_once('functions-formidable.php');

// this function has been run once, but the changes are permanently made in the
// database. Replace add_cap with remove_cap to reverse these changes.
// function add_theme_caps() {
//     $role = get_role( 'site_editor' );
//     $role->add_cap( 'promote_users' ); 
// }
// add_action( 'admin_init', 'add_theme_caps');

// Keep users logged into the Production Hub
add_filter( 'auth_cookie_expiration', 'keep_me_logged_in_for_1_year' );

function keep_me_logged_in_for_1_year( $expirein ) {
    return 31556926; // 1 year in seconds
}

// Add message above login form 
function wpsd_add_login_message() {
	return '<p class="message">For security and auditing purposes, certain user actions on this site are recorded in an audit log. The audit log also includes the IP address where you accessed this site from.</p> <p class="message">Production Hub can be veiwed by registered users of this stie only. <a href="mailto:qualitymanagement@tandf.co.uk">Request access to the Production Hub</a></p>';
}
add_filter('login_message', 'wpsd_add_login_message');


// Hide search button
add_filter( 'bop_nav_search_show_submit_button', function( $bool, $item, $depth, $args ){
  $bool = false;
  return $bool;
}, 10, 4 );


?>